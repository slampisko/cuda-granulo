cuda-granulo - GPU accelerated granulodescriptor.
The goal of this program was to create a GPU implementation of morphologic
granulodescriptor and compare it with an existing CPU implementation.

Usage:

$> ./cuda-granulo [-h|-c|-s|-n X|-w] <input file>

   -h[elp]     Show this usage information.
   -c[pu]      if present, indicates that the CPU implementation should run too
               (for comparison).
   -s[upress]  if present, supresses the execution of the GPU implementation
               (implies -c).
   -n[umber] X if present, overrides the default number of openings (25)
               and sets it to the supplied value.
   -w[ait]     waits for the user to press Enter after execution is completed
               before closing the program.

EXAMPLE 1:
$> ./cuda-granulo obrazek.png

EXAMPLE 2:
$> ./cuda-granulo -c -n 16 obrazek.png
