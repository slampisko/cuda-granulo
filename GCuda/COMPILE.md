Prerequisites to build:

Windows:
-  CUDA SDK installed and configured for Visual Studio
-  i3dcore and i3dalgo library binaries in project directory (copy the contents of "include" and "lib" into the folders "GCuda/include" and "GCuda/lib")
   [http://cbia.fi.muni.cz/projects/the-cbia-i3dcore-and-i3dalgo-libraries.html]
-> compile using Visual Studio (tested on 2012)
  
Linux:
-  CUDA SDK installed and configured
-  i3dcore and i3dalgo libraries in /usr/local/include/i3d
-> run "make"



Prerequisites to run:

Windows:
-  i3dcore and i3dalgo library binaries (*.dll) in PATH or in same directory (you can put them in the GCuda/bin folder to have them copied to output on build)
-  other CBIA libraries (fftw, hdf5, ics, jpeg, lapack+blas, png, regex, tiff, zlib) in PATH or in same directory
-> run cuda-granulo.exe

Linux:
-  i3dcore and i3dalgo libraries installed
-  other CBIA libraries (fftw, hdf5, ics, jpeg, lapack+blas, png, regex, tiff, zlib) installed
-> run .\cuda-granulo