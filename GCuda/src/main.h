#ifndef _TEST_HEADER
#define _TEST_HEADER

template <class T> void ComputeGranulo2(i3d::Image3d<T> &img, std::string filename);
template <class T> i3d::Image3d<T> Open(i3d::Image3d<T> &img);
template <class T> i3d::Image3d<T> Erode(i3d::Image3d<T> &img);

void usage(const char *str);

int main (int argc, char *argv[]);

int checkCudaSupport();

#endif