#ifndef _GCUDA_HEADER
#define _GCUDA_HEADER
    namespace gcuda {
        void GranuloGpuGray8(i3d::Image3d<i3d::GRAY8> img, const unsigned int numberOfOpenings);
        void GranuloGpuGray16(i3d::Image3d<i3d::GRAY16> img, const unsigned int numberOfOpenings);
        i3d::Image3d<i3d::GRAY8> OpenGpuGray8(i3d::Image3d<i3d::GRAY8> img, const unsigned int radius);
        i3d::Image3d<i3d::GRAY16> OpenGpuGray16(i3d::Image3d<i3d::GRAY16> img, const unsigned int radius);
    }
#endif
