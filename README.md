Repo readme. For program readme go to the GCuda subfolder.

The goal of this program was to create a GPU implementation of morphologic granulodescriptor and compare it with an existing CPU implementation.

All binary releases for Windows are available at the following link:

[https://drive.google.com/folderview?id=0B8iCwaY23uYyaGR1WXFvRzBTOUU](Google Drive – Downloads for cuda-granulo)

Recent releases:
[https://drive.google.com/uc?export=download&id=0B8iCwaY23uYyd3hqOUlMc1c1WVU](GCudaWin32-1.1-Release.zip)

```
Do note that the releases are missing the i3d libraries. On Windows, this means namely the following DLLs (not all of them might be needed, but it's what makes it run on my machine):

cbia.lib.blas.dyn.rel.x86.11.dll
cbia.lib.fftw.dyn.rel.x86.11.dll
cbia.lib.fftwf.dyn.rel.x86.11.dll
cbia.lib.fftwl.dyn.rel.x86.11.dll
cbia.lib.glew.dyn.rel.x86.11.dll
cbia.lib.hdf5.dyn.rel.x86.11.dll
cbia.lib.hdf5_hl.dyn.rel.x86.11.dll
cbia.lib.i3dalgo.dyn.rel.x86.11.dll
cbia.lib.i3dcore.dyn.rel.x86.11.dll
cbia.lib.ics.dyn.rel.x86.11.dll
cbia.lib.jpeg.dyn.rel.x86.11.dll
cbia.lib.lapack.dyn.rel.x86.11.dll
cbia.lib.png.dyn.rel.x86.11.dll
cbia.lib.regex.dyn.rel.x86.11.dll
cbia.lib.tiff.dyn.rel.x86.11.dll
cbia.lib.z.dyn.rel.x86.11.dll
cudart32_55.dll
cudart64_55.dll
libifcoremd.dll
libmmd.dll
msvcp110.dll
msvcr110.dll
```

Please go to the GCuda subfolder and see COMPILE.md for build instructions and README.md for usage instructions.
